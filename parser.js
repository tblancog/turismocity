class Parser {

  parseResponse(response, trackingParams) {
    const itineraries = [];
    const utmCampaign = this.getParameterByName('utm_campaign', trackingParams);
    const newUtmCampaign = utmCampaign.replace('-', '_');
    response.forEach((item) => {
      if (item.hasOwnProperty('flights') && item.flights.length > 0) {
        if (item.bookURL.indexOf(newUtmCampaign) >= 0) {
          const providers = [];
          providers.push({
            bookURL: `${item.bookURL}?${trackingParams}`,
            price: this.getLowestPrice(item.price),
            providerName: this.parseProvider(item.providerName),
          });
          const segments = {
            outbound: {
              flights: this.getFlights(item.flights, 'outbound'),
            },
            inbound: {
              flights: this.getFlights(item.flights, 'inbound'),
            },
          };

          item.flights.forEach(() => {
            itineraries.push({
              providers,
              segments,
            });
          });
        }
      }
    });
    return {
      itineraries,
    };
  }

  getFlights(flights, type) {
    const newFlights = flights.filter((flight) => {
      if (type === 'outbound') {
        return flight.departure === 'EZE';
      }
    });
    return newFlights;
  }
  getLowestPrice(prices) {
    const arr = prices.map((price) => {
      return price.value;
    });
    return Math.max.apply(null, arr);
  }

  getParameterByName(name, url) {
    const newName = name.replace(/[\[\]]/g, '\\$&');
    // const regex = new RegExp('[?&]' + newName + '(=([^&#]*)|&|#|$)');
    const regex = new RegExp('[?&]' + newName + '(=([^&#]*)|&|#|$)');
    const results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  parseProvider(name) {
    const newName = name.split('.COM')[0];
    return `${newName.charAt(0)}${newName.substr(1, newName.length).toLowerCase()}`;
  }

}

exports.helper = new Parser();

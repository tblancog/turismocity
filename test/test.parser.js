/* global it */
/* global before */
/* global describe */
const expect = require('chai').expect;
const assert = require('chai').assert;

const CheaptotripCrawler = require('../index.js').Crawler;

const searchParams = {
  countryCode: 'AR',
  fromLocation: 'BUE',
  toLocation: 'RIO',
  cabinClass: 'Economy',
  outboundDate: '2015-7-27',
  inboundDate: '2015-8-6',
  numAdults: 1,
  numChildren: 0,
  tripType: 'RoundTrip',
  numInfants: 0,
};

const fs = require('fs');
const expected = JSON.parse(fs.readFileSync('./test/cheaptotrip.out', 'utf8'));

let results;

describe('Turismocity simple parser', () => {
  before((done) => {
    const crawler = new CheaptotripCrawler(searchParams);

    crawler.on('pulled', (data) => {
      results = data;
    })
    .on('ready', done);
  });

  it('shoud have itineraries', (done) => {
    assert.ok(results.itineraries.length > 0, 'no hay itinerarios');
    assert.equal(results.itineraries.length, expected.itineraries.length,
			'Cantidad de resultados no es la esperada');
    done();
  });

  it('should like expected response itinerary', (done) => {
    expect(results.itineraries[0]).to.deep.equal(expected.itineraries[0]);
    done();
  });
});
